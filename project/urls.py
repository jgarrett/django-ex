from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from welcome.views import index, health
from linewashapp.views import *
from django.contrib.auth import views as auth_views

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', home),
    url(r'^add', add),
    url(r'^show', show),
    url(r'^edit/(\d+)/', edit, name = 'id'),
    url(r'^update/(\d+)/', update, name = 'id'),
    url(r'^delete/(\d+)/', delete, name = 'id'),

    url(r'^edit/car/(\d+)/', editCar, name = 'id'),
    url(r'^update/car/(\d+)/', updateCar, name = 'id'),
    
    url(r'^edit/client/(\d+)/', editClient, name = 'id'),
    url(r'^update/client/(\d+)/', updateClient, name = 'id'),

    url(r'^price/(\d+)/(\d+)/(\d+)/', price),

    url(r'^test/', bootstrapHome),
    url(r'^health$', health),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^accounts/logout/$', auth_views.LogoutView.as_view(), {'next_page': '/'}),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns




