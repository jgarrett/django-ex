import os
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse, QueryDict, JsonResponse

from datetime import datetime

from . import database
from .models import PageView

from django.contrib.auth.decorators import login_required

from linewashapp.forms import WashForm, ClientForm, CarForm
from linewashapp.models import *

# Create your views here.

def getCurrentDate():
   now = datetime.now()
#    dt_string = now.strftime("%d/%m/%Y %H:%M")
   dt_string = now.strftime("%Y-%m-%d %H:%M")
   return dt_string

def getCarBrands():
	return ["Alfa Romeo", "Aston Martin", "Audi", "Bentley", "BMW", "Bugatti", "Cadillac", "Chevrolet", "Chrysler", "Citroen", "Corvette", "DAF", "Dacia", "Daewoo", "Daihatsu", "Datsun", "Dino", "Dodge", "Farboud", "Ferrari", "Fiat", "Ford", "Honda", "Hummer", "Hyundai", "Jaguar", "Jeep", "KIA", "Koenigsegg", "Lada", "Lamborghini", "Lancia", "Land Rover", "Lexus", "Ligier", "Lincoln", "Lotus", "Martini", "Maserati", "Maybach", "Mazda", "McLaren", "Mercedes-Benz", "Mini", "Mitsubishi", "Nissan", "Noble", "Opel", "Peugeot", "Pontiac", "Porsche", "Renault", "Rolls-Royce", "Rover", "Saab", "Seat", "Skoda", "Smart", "Spyker", "Subaru", "Suzuki", "Toyota", "Vauxhall", "Volkswagen", "Volvo", "Outro"]

@login_required
def home(request):
   return render(request, "home.html", {})

@login_required
def add(request):
    if request.method == "POST":
        plate = request.POST.get('plate', False)
        
        email = request.POST.get('email', False)
        nif = request.POST.get('nif', False)
        # name = request.POST.get('name', False)
        
        car = None
        client = None
        
        if (plate):
            try:
                car = Car.objects.get(plate__iexact=plate)
            except Car.DoesNotExist:
                carForm = CarForm(request.POST)
                try:
                    if carForm.is_valid():
                        car = carForm.save()
                    else:
                        print('Car form is not valid')
                        print(carForm.errors)
                except Exception as e:
                    print(e)
                    pass
        
        if (email or nif):
            try:
                if (nif):
                    client = Client.objects.get(nif__iexact = nif)
                else:
                    client = Client.objects.get(email__iexact = email)
            except Client.DoesNotExist:
                clientForm = ClientForm(request.POST)
                try:
                    if clientForm.is_valid():
                        client = clientForm.save()
                    else:
                        print('Client form is not valid')
                        print(clientForm.errors)
                except Exception as e:
                    print(e)
                    pass

        washForm = WashForm(request.POST)
        if washForm.is_valid():
            try:  
                wash = washForm.save(commit=False)  
                wash.car = car
                wash.client = client
                wash = washForm.save()  
                return redirect('/show')  
            except Exception as e:
                print(e)
                pass
        else:
            print("\nForms are not valid.\n")
            print(washForm.errors)
    
    paymentType = PaymentType.objects.get(name='Dinheiro')
    clientType = ClientType.objects.get(name='Cliente Premium')
    serviceType = ServiceType.objects.get(name='Completa')
    carType = CarType.objects.get(name='Ligeiro')

    washForm = WashForm(initial = {'paymentType' : paymentType.id, 'clientType' : clientType.id, 'serviceType' : serviceType.id})
    clientForm = ClientForm()
    carForm = CarForm(initial = {'carType' : carType.id})
    return render(request, "add.html", {'carBrands' : getCarBrands() , 'dateStr': getCurrentDate() , 'washForm' : washForm, 'clientForm' : clientForm, 'carForm': carForm, 'washModel' : Wash() })

@login_required
def show(request):  
    washes = Wash.objects.all()  
    return render(request,"show.html",{'washes' : washes})  

@login_required
def edit(request, id):
    try:
        wash = Wash.objects.get(id=id)  
        form = WashForm(instance = wash)  
    except Wash.DoesNotExist:
        return redirect("/add")
    return render(request,'edit.html', {'wash' : wash, 'washForm' : form})  

@login_required
def update(request, id):
    try:
        wash = Wash.objects.get(id=id)
        values = QueryDict(mutable=True)
        values.update(request.POST)
        values.update({'car': wash.car.id})
        if wash.car != None:
            values.update({'car': wash.car.id})
        if wash.client != None:
            values.update({'client': wash.client.id})
        form = WashForm(values, instance = wash)
        if form.is_valid():
            form.save()  
            return redirect("/show")
        else:
            print(form.errors)
    except Wash.DoesNotExist:
        pass
    return redirect("/edit/" + id)  

@login_required
def delete(request, id):  
    wash = Wash.objects.get(id=id)  
    wash.delete()  
    return redirect("/show")

@login_required
def editCar(request, id):
    try:
        car = Car.objects.get(id=id)  
        form = CarForm(instance = car)  
    except Car.DoesNotExist:
        return redirect("/add")
    return render(request,'editCar.html', {'carBrands' : getCarBrands(), 'car' : car, 'carForm' : form})  

@login_required
def editClient(request, id):
    try:
        client = Client.objects.get(id=id)  
        form = ClientForm(instance = client)  
    except Client.DoesNotExist:
        return redirect("/add")
    return render(request,'editClient.html', {'client' : client, 'clientForm' : form})  

@login_required
def updateCar(request, id):
    try:
        car = Car.objects.get(id=id)
        form = CarForm(request.POST, instance = car)
        if form.is_valid():
            form.save()  
            return redirect("/show")
        else:
            print(form.errors)
    except Car.DoesNotExist:
        pass
    return redirect("/edit/car/" + id)

@login_required
def updateClient(request, id):
    try:
        client = Client.objects.get(id=id)
        form = ClientForm(request.POST, instance = client)
        if form.is_valid():
            form.save()  
            return redirect("/show")
        else:
            print(form.errors)
    except Client.DoesNotExist:
        pass
    return redirect("/edit/client/" + id)

@login_required
def health(request):
    return HttpResponse(PageView.objects.count())

@login_required
def bootstrapHome(request):
   return render(request, "home_test.html", {})

def price(request,serviceTypeId, clientTypeId, carTypeId):
    priceObj = Price.objects.get(carType_id=carTypeId,serviceType_id=serviceTypeId,clientType_id=clientTypeId)
    responseData = {
        'price': priceObj.price,
        'clientTypeId': clientTypeId,
        'serviceTypeId' : serviceTypeId,
        'carTypeId' : carTypeId,
    }
    return JsonResponse(responseData)