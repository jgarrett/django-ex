from django import forms  
from linewashapp.models import Wash, Car, Client

class WashForm(forms.ModelForm):  
    class Meta:  
        model = Wash
        fields = "__all__"
        widgets = {
            'paymentType': forms.Select(attrs={'class': 'custom-select d-block w-100'}),
            'clientType': forms.Select(attrs={'class': 'custom-select d-block w-100 price-update'}),
            'serviceType': forms.Select(attrs={'class': 'custom-select d-block w-100 price-update'}),
        }

class CarForm(forms.ModelForm):  
    class Meta:  
        model = Car
        fields = "__all__"
        widgets = {
            'carType': forms.Select(attrs={'class': 'custom-select d-block w-100 price-update'}),
        }

class ClientForm(forms.ModelForm):  
    class Meta:  
        model = Client
        fields = "__all__"