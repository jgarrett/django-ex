from __future__ import unicode_literals

from django.db import migrations
from django.db import models
from linewashapp.models import *

def addPricesTable(apps, schema_editor):
    smart = CarType.objects.get(name='Smart')
    ligeiro = CarType.objects.get(name='Ligeiro')
    sw = CarType.objects.get(name='SW / Jipe')
    mono = CarType.objects.get(name='Monovolume')
    mota = CarType.objects.get(name='Mota')

    client = ClientType.objects.get(name='Cliente')
    premium = ClientType.objects.get(name='Cliente Premium')
    partner = ClientType.objects.get(name= 'Parcerias')

    exterior = ServiceType.objects.get(name='Exterior')
    interior = ServiceType.objects.get(name='Interior')
    full = ServiceType.objects.get(name='Completa')
    seats = ServiceType.objects.get(name='Estofos')
    prestige = ServiceType.objects.get(name='Prestige')
    hidrate = ServiceType.objects.get(name='Hidratação')
    engine = ServiceType.objects.get(name='Motor')
    rugs = ServiceType.objects.get(name='Alcatifas')
    optical = ServiceType.objects.get(name='Ópticas')
    polish = ServiceType.objects.get(name='Polimento Completo')

    serviceMapping = {
        0	: exterior,
        1	: interior,
        2	: full,
        3	: seats,
        4	: prestige,
        5	: hidrate,
        6	: engine,
        7	: rugs,
        8	: optical,
        9	: polish
    }

    carMapping = {
        0	: smart,
        1	: ligeiro,
        2	: sw,
        3	: mono,
        4	: mota,
    }

    clientMapping = {
        'client'	: client,
        'premium'	: premium,
        'partners'	: partner,
    }

    pricesClient = {
        0   :   [6.50,6.50,10.00,20.00,80.00,12.50,25.00,15.00,10.00,60.00],
        1   :   [8.00,8.00,12.50,40.00,100.00,22.50,25.00,27.00,10.00,100.00],
        2   :   [10.00,10.00,15.00,45.00,110.00,22.50,25.00,35.00,10.00,110.00],
        3   :   [12.50,12.50,17.50,55.00,130.00,30.00,25.00,47.00,10.00,140.00],
        4   :   [8.00,0.00,0.00,0.00,0.00,8.00,0.00,0.00,0.00,0.00],
    }

    pricesClientPremium = {
        0   :   [7.50,7.50,12.50,25.00,90.00,15.00,35.00,17.50,15,75.00],
        1   :   [10.00,10.00,15.00,45.00,110.00,25.00,35.00,30.00,15,110.00],
        2   :   [12.00,12.00,17.00,50.00,120.00,25.00,35.00,40.00,15,120.00],
        3   :   [14.00,14.00,20.00,60.00,140.00,35.00,35.00,55.00,15,150.00],
        4   :   [10.00,0.00,0.00,0.00,0.00,10.00,0.00,0.00,0.00,0.00],
    }

    pricesPartners = {
        0   :   [5.00,5.00,10.00,20.00,80.00,12.50,20.00,15.00,10.00,60.00],
        1   :   [5.00,5.00,10.00,40.00,100.00,22.50,15.00,27.00,10.00,100.00],
        2   :   [7.50,7.50,12.50,45.00,110.00,22.50,15.00,35.00,10.00,110.00],
        3   :   [10.00,10.00,15.00,55.00,130.00,30.00,15.00,47.00,10.00,140.00],
        4   :   [8.00,0.00,0.00,0.00,0.00,8.00,0.00,0.00,0.00,0.00],
    }

    prices = {
        'client' : pricesClient,
        'premium' : pricesClientPremium,
        'partners' : pricesPartners
    }

    for clientTypeIndex, pricesByClient in prices.items():
        clientType = clientMapping[clientTypeIndex]
        for carTypeIndex, priceList in pricesByClient.items():
            carType = carMapping[carTypeIndex]
            for index in range(0, len(priceList)):
                price = Price()
                price.price = priceList[index]
                price.clientType = clientType
                price.carType = carType
                price.serviceType = serviceMapping[index]
                price.save()
            
class Migration(migrations.Migration):

    dependencies = [
        ('linewashapp', '0012_fixTypes'),
    ]

    operations = [
        migrations.RunPython(addPricesTable)
    ]