from __future__ import unicode_literals

from django.db import migrations
from django.db import models
from linewashapp.models import ClientType, CarType

def fixClientTypes(apps, schema_editor):
    types = ["Cliente","Cliente Premium", "Parcerias"]
    ClientType.objects.all().delete()
    for clientType in types:
        newClientType = ClientType()
        newClientType.name = clientType
        newClientType.save()

def addCarTypes(apps, schema_editor):
    types = ["Smart","Ligeiro","SW / Jipe", "Monovolume","Mota" ]
    for carType in types:
        newCarType = CarType()
        newCarType.name = carType
        newCarType.save()

class Migration(migrations.Migration):

    dependencies = [
        ('linewashapp', '0011_populate_2'),
    ]

    operations = [
        migrations.RunPython(fixClientTypes),
        migrations.RunPython(addCarTypes),
    ]