from django.db import models
from django.utils import timezone

# Create your models here.

class PageView(models.Model):
    hostname = models.CharField(max_length = 50)
    timestamp = models.DateTimeField(auto_now_add = True)

class ClientType(models.Model):
    name = models.CharField(max_length = 100, null = True, blank = True)
    def  __str__(self):
        return self.name

class ServiceType(models.Model):
    name = models.CharField(max_length = 100, null = True, blank = True)
    def  __str__(self):
        return self.name

class CarType(models.Model):
    name = models.CharField(max_length = 100, null = True, blank = True)
    def  __str__(self):
        return self.name

class PaymentType(models.Model):
    name = models.CharField(max_length = 100, null = True, blank = True)
    def  __str__(self):
        return self.name

class Price(models.Model):
    clientType = models.ForeignKey(ClientType, null = True, blank = True, verbose_name = "ClientType", on_delete = models.SET_NULL)
    serviceType = models.ForeignKey(ServiceType, null = True, blank = True, verbose_name = "ServiceType", on_delete = models.SET_NULL)
    carType = models.ForeignKey(CarType, null = True, blank = True, verbose_name = "CarType", on_delete = models.SET_NULL)
    price = models.FloatField(null = True, blank = True)
    def  __str__(self):
        return self.name

class Car(models.Model):
    brand = models.CharField(max_length = 50, null = True, blank = True) 
    model = models.CharField(max_length = 50, null = True, blank = True)
    plate = models.CharField(max_length = 20,unique = True)
    carType = models.ForeignKey(CarType, null = True, blank = True, verbose_name = "CarType", on_delete = models.SET_NULL)
    class Meta:
        db_table = "car"

class Client(models.Model):
    phone = models.CharField(max_length = 50, unique = True, null = True, blank = True)
    name = models.CharField(max_length = 50, null = True, blank = True)
    birthday = models.CharField(max_length = 50, null = True, blank = True)
    nif = models.CharField(max_length = 50,unique = True, null = True, blank = True)
    email = models.CharField(max_length = 50, null = True, blank = True)
    address = models.CharField(max_length = 400, null = True, blank = True)
    class Meta:
        db_table = "client"

class Wash(models.Model):
    timestamp = models.DateTimeField(default = timezone.now,blank = True)
    price = models.FloatField(null = True, blank = True)
    tip = models.FloatField(default = 0,null = True, blank = True)
    finalClient = models.BooleanField(default = True,blank = True)
    payed = models.BooleanField(default = True,blank = True)
    comments = models.CharField(max_length = 1000, null = True, blank = True)
    car = models.ForeignKey(Car, null = True, blank = True, verbose_name = "Car", on_delete = models.SET_NULL)
    client = models.ForeignKey(Client, null = True, blank = True, verbose_name = "Client", on_delete = models.SET_NULL)
    paymentType = models.ForeignKey(PaymentType, null = True, blank = True, verbose_name = "PaymentType", on_delete = models.SET_NULL)
    clientType = models.ForeignKey(ClientType, null = True, blank = True, verbose_name = "ClientType", on_delete = models.SET_NULL)
    serviceType = models.ForeignKey(ServiceType, null = True, blank = True, verbose_name = "ServiceType", on_delete = models.SET_NULL)
    class Meta:
        db_table = "wash"